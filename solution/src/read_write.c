#include "read_write.h"

bool read_uint8_t(FILE* file, uint8_t * output) {
    if (feof(file))
        return false;
    *output = fgetc(file);
    return !ferror(file);
}

bool read_le_uint16_t(FILE* file, uint16_t * output) {
    uint8_t b0, b1;
    if (!read_uint8_t(file, &b0))
        return false;
    if (!read_uint8_t(file, &b1))
        return false;
    *output = (uint16_t)b0 |
              (uint16_t)b1 << 8;
    return true;
}

bool read_le_uint32_t(FILE* file, uint32_t * output)  {
    uint8_t b0, b1, b2, b3;
    if (!read_uint8_t(file, &b0))
        return false;
    if (!read_uint8_t(file, &b1))
        return false;
    if (!read_uint8_t(file, &b2))
        return false;
    if (!read_uint8_t(file, &b3))
        return false;
    *output = (uint32_t)b0 |
              (uint32_t)b1 << 8 |
              (uint32_t)b2 << 16 |
              (uint32_t)b3 << 24;
    return true;
}

bool write_uint8_t(FILE* file, uint8_t input) {
    if (fputc(input, file) == EOF)
        return false;
    return !ferror(file);
}

bool write_le_uint16_t(FILE* file, uint16_t input) {
    if (!write_uint8_t(file, (uint8_t)(input & 0xff)))
        return false;
    if (!write_uint8_t(file, (uint8_t)(input >> 8 & 0xff)))
        return false;
    return true;
}

bool write_le_uint32_t(FILE* file, uint32_t input) {
    if (!write_uint8_t(file, (uint8_t)(input & 0xff)))
        return false;
    if (!write_uint8_t(file, (uint8_t)(input >> 8 & 0xff)))
        return false;
    if (!write_uint8_t(file, (uint8_t)(input >> 16 & 0xff)))
        return false;
    if (!write_uint8_t(file, (uint8_t)(input >> 24 & 0xff)))
        return false;
    return true;
}
