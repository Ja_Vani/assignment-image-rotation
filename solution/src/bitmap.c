#include "bitmap.h"
#include "read_write.h"
#include <stdlib.h>

#define type 0x4d42

const char* bitmap_errmsg[] = {
    [BMP_NO_ERROR] = "No error",
    [BMP_FILE_READ_ISSUE] = "Cannot read file",
    [BMP_FILE_WRITE_ISSUE] = "Cannot write file",
    [BMP_WRONG_SIGNATURE] = "Wrong file signature",
    [BMP_NO_RESERVED] = "No reserved bits",
    [BMP_UNSUPPORTED_OFFSET] = "Unsupported offset",
    [BMP_UNSUPPORTED_INFO_FORMAT] = "Unsupported info format",
    [BMP_UNSUPPORTED_PLANES_COUNT] = "Unsupported planes count",
    [BMP_UNSUPPORTED_BITS_COUNT] = "Unsupported bits count",
    [BMP_UNSUPPORTED_COMPRESSION] = "Unsupported compression",
    [BMP_UNSUPPORTED_CLR] = "Unsupported info CLR",
    [BMP_WONG_SIZE] = "Wrong size",
    [BMP_WONG_FILESIZE] = "Wrong file size",
    [BMP_WRONG_PADDINGS] = "Wrong paddings",
};

struct bitmap* create_blank_bitmap() {
    struct bitmap* result = malloc(sizeof(struct bitmap));
    result->header.bfType = 0;
    result->header.bfileSize = 0;
    result->header.bfReserved = 0;
    result->header.bOffBits = 0;
    result->header.biSize = 0;
    result->header.biWidth = 0;
    result->header.biHeight = 0;
    result->header.biPlanes = 0;
    result->header.biBitCount = 0;
    result->header.biCompression = 0;
    result->header.biSizeImage = 0;
    result->header.biXPelsPerMeter = 0;
    result->header.biYPelsPerMeter = 0;
    result->header.biClrUsed = 0;
    result->header.biClrImportant = 0;
    result->data = NULL;
    return result;
}

enum bitmap_errors try_read_bitmap(struct bitmap* bmp, FILE* file) {
    if (!( read_le_uint16_t(file, &(bmp->header.bfType)) &&
           read_le_uint32_t(file, &(bmp->header.bfileSize)) &&
           read_le_uint32_t(file, &(bmp->header.bfReserved)) &&
           read_le_uint32_t(file, &(bmp->header.bOffBits)) &&
           read_le_uint32_t(file, &(bmp->header.biSize)) &&
           read_le_uint32_t(file, &(bmp->header.biWidth)) &&
           read_le_uint32_t(file, &(bmp->header.biHeight)) &&
           read_le_uint16_t(file, &(bmp->header.biPlanes)) &&
           read_le_uint16_t(file, &(bmp->header.biBitCount)) &&
           read_le_uint32_t(file, &(bmp->header.biCompression)) &&
           read_le_uint32_t(file, &(bmp->header.biSizeImage)) &&
           read_le_uint32_t(file, &(bmp->header.biXPelsPerMeter)) &&
           read_le_uint32_t(file, &(bmp->header.biYPelsPerMeter)) &&
           read_le_uint32_t(file, &(bmp->header.biClrUsed)) &&
           read_le_uint32_t(file, &(bmp->header.biClrImportant)) ))
        return BMP_FILE_READ_ISSUE;

    if (bmp->header.bfType != type)
        return BMP_WRONG_SIGNATURE;
    if (bmp->header.bfReserved != 0)
        return BMP_NO_RESERVED;
    if (bmp->header.bOffBits != sizeof(struct bitmap_header))
        return BMP_UNSUPPORTED_OFFSET;
    if (bmp->header.biSize != sizeof(struct bitmap_header) - 14)
        return BMP_UNSUPPORTED_INFO_FORMAT;
    if (bmp->header.biPlanes != 1)
        return BMP_UNSUPPORTED_PLANES_COUNT;
    if (bmp->header.biBitCount != 8 * sizeof(struct bitmap_pixel))
        return BMP_UNSUPPORTED_BITS_COUNT;
    if (bmp->header.biCompression != 0)
        return BMP_UNSUPPORTED_COMPRESSION;
    if (bmp->header.biClrUsed != 0)
        return BMP_UNSUPPORTED_CLR;
    if (bmp->header.biClrImportant != 0)
        return BMP_UNSUPPORTED_CLR;

    uint32_t w, h, s;
    w = bmp->header.biWidth;
    h = bmp->header.biHeight;
    s = bmp->header.biSizeImage;
    if ((3 * w + w % 4) * h > s)
        return BMP_WONG_SIZE;
    if (bmp->header.bfileSize - bmp->header.biSizeImage != sizeof(struct bitmap_header))
        return BMP_WONG_FILESIZE;

    bmp->data = malloc(sizeof(struct bitmap_pixel) * w * h);

    uint8_t buffer;
    for (uint32_t y = 0; y < h; y++) {
        for (uint32_t x = 0; x < w; x++) {
            if(!read_uint8_t(file, &(get_pixel_ref(bmp, x, y)->r)))
                return BMP_FILE_READ_ISSUE;
            if(!read_uint8_t(file, &(get_pixel_ref(bmp, x, y)->g)))
                return BMP_FILE_READ_ISSUE;
            if(!read_uint8_t(file, &(get_pixel_ref(bmp, x, y)->b)))
                return BMP_FILE_READ_ISSUE;
        }
        for(uint32_t add = 0; add < w % 4; add++) {
            read_uint8_t(file, &buffer);
            if (buffer != 0)
                return BMP_WRONG_PADDINGS;
        }
    }
    return BMP_NO_ERROR;
}

enum bitmap_errors try_write_bitmap(struct bitmap* bmp, FILE* file) {
    bmp->header.bfType = type;
    bmp->header.bfReserved = 0;
    bmp->header.bOffBits = sizeof(struct bitmap_header);
    bmp->header.biSize = sizeof(struct bitmap_header) - 14;
    bmp->header.biPlanes = 1;
    bmp->header.biBitCount = sizeof(struct bitmap_pixel) * 8;
    bmp->header.biCompression = 0;
    bmp->header.biSizeImage =
        (3 * bmp->header.biWidth + bmp->header.biWidth % 4)
        * bmp->header.biHeight;

    // IDK why they are set to 0 in the expected output
    bmp->header.biXPelsPerMeter = 0;
    bmp->header.biYPelsPerMeter = 0;

    bmp->header.biClrUsed = 0;
    bmp->header.biClrImportant = 0;
    bmp->header.bfileSize = bmp->header.biSizeImage + sizeof(struct bitmap_header);


    if (!( write_le_uint16_t(file, bmp->header.bfType) &&
           write_le_uint32_t(file, bmp->header.bfileSize) &&
           write_le_uint32_t(file, bmp->header.bfReserved) &&
           write_le_uint32_t(file, bmp->header.bOffBits) &&
           write_le_uint32_t(file, bmp->header.biSize) &&
           write_le_uint32_t(file, bmp->header.biWidth) &&
           write_le_uint32_t(file, bmp->header.biHeight) &&
           write_le_uint16_t(file, bmp->header.biPlanes) &&
           write_le_uint16_t(file, bmp->header.biBitCount) &&
           write_le_uint32_t(file, bmp->header.biCompression) &&
           write_le_uint32_t(file, bmp->header.biSizeImage) &&
           write_le_uint32_t(file, bmp->header.biXPelsPerMeter) &&
           write_le_uint32_t(file, bmp->header.biYPelsPerMeter) &&
           write_le_uint32_t(file, bmp->header.biClrUsed) &&
           write_le_uint32_t(file, bmp->header.biClrImportant) ))
        return BMP_FILE_WRITE_ISSUE;

    for (uint32_t y = 0; y < bmp->header.biHeight; y++) {
        for (uint32_t x = 0; x < bmp->header.biWidth; x++) {
            if(!write_uint8_t(file, get_pixel_ref(bmp, x, y)->r))
                return BMP_FILE_WRITE_ISSUE;
            if(!write_uint8_t(file, get_pixel_ref(bmp, x, y)->g))
                return BMP_FILE_WRITE_ISSUE;
            if(!write_uint8_t(file, get_pixel_ref(bmp, x, y)->b))
                return BMP_FILE_WRITE_ISSUE;
        }
        for(uint32_t add = 0; add < bmp->header.biWidth % 4; add++) {
            write_uint8_t(file, 0);
        }
    }
    return BMP_NO_ERROR;
}

void destroy_bitmap(struct bitmap* bmp) {
    free(bmp->data);
    bmp->data = NULL;
    free(bmp);
}

struct bitmap_pixel* get_pixel_ref(struct bitmap* bmp, uint32_t x, uint32_t y) {
    if (bmp->data == NULL)
        return NULL;
    return bmp->data + y * bmp->header.biWidth + x;
}


void mirror_bmp_x(struct bitmap* bmp) {
    for (uint32_t y = 0; y < bmp->header.biHeight; y++) {
        for (uint32_t x = 0; x < bmp->header.biWidth / 2; x++) {
            struct bitmap_pixel* a = get_pixel_ref(bmp, x, y);
            struct bitmap_pixel* b = get_pixel_ref(bmp, bmp->header.biWidth - x - 1, y);
            struct bitmap_pixel tmp = *a;
            *a = *b;
            *b = tmp;
        }
    }
}

void transpose_bmp(struct bitmap* bmp) {
    struct bitmap_pixel* old_data = malloc(sizeof(struct bitmap_pixel)
        * bmp->header.biWidth * bmp->header.biHeight);
    for (uint32_t y = 0; y < bmp->header.biHeight; y++) {
        for (uint32_t x = 0; x < bmp->header.biWidth; x++) {
            old_data[y * bmp->header.biWidth + x] = *get_pixel_ref(bmp, x, y);
        }
    }

    const uint32_t tmp = bmp->header.biHeight;
    bmp->header.biHeight = bmp->header.biWidth;
    bmp->header.biWidth = tmp;

    for (uint32_t xtoy = 0; xtoy < bmp->header.biHeight; xtoy++) {
        for (uint32_t ytox = 0; ytox < bmp->header.biWidth; ytox++) {
            *get_pixel_ref(bmp, ytox, xtoy) = old_data[ytox * bmp->header.biHeight + xtoy];
        }
    }
    free(old_data);
}
