#include "bitmap.h"
#include <stdio.h>

int file_read(struct bitmap* bmp, char** argv){
    FILE* input = fopen(argv[1], "rb");
    if (!input) {
        fprintf(stderr, "Failed to open file %s\n", argv[1]);
        return 4;
    }

    enum bitmap_errors read_result = try_read_bitmap(bmp, input);
    if (read_result) {
        fprintf(stderr, "Failed to read file %s: %s\n", argv[1], bitmap_errmsg[read_result]);
        return 2;
    }
    fclose(input);
    return 0;
}

int file_write(struct bitmap* bmp, char** argv){
    FILE* output = fopen(argv[2], "wb");
    if (!output) {
        fprintf(stderr, "Failed to open file %s\n", argv[2]);
        return 4;
    }

    enum bitmap_errors write_result = try_write_bitmap(bmp, output);
    if (write_result) {
        fprintf(stderr, "Failed to write into file %s: %s\n", argv[2], bitmap_errmsg[write_result]);
        return 3;
    }
    fclose(output);
    return 0;
}

int main( int argc, char** argv ) {
    if (argc < 2) {
        fprintf(stderr, "Error! No input file.\n");
        return 1;
    }
    if (argc < 3) {
        fprintf(stderr, "Error! No output file.\n");
        return 1;
    }

    struct bitmap* bmp = create_blank_bitmap();

    switch(file_read(bmp, argv)){
        case 2: return 2;
        case 4: return 4;
    }

    transpose_bmp(bmp);
    mirror_bmp_x(bmp);

    switch(file_write(bmp, argv)){
        case 3: return 3;
        case 4: return 4;
    }

    destroy_bitmap(bmp);

    fprintf(stderr, "Success");
    return 0;
}
