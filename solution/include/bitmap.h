#ifndef BITMAP_H
#define BITMAP_H

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#pragma pack(push, 1)

struct bitmap_pixel {
    uint8_t r;
    uint8_t g;
    uint8_t b;
};

struct bitmap_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

struct bitmap {
    struct bitmap_header header;
    struct bitmap_pixel* data;
};

#pragma pack(pop)

enum bitmap_errors {
    BMP_NO_ERROR = 0,
    BMP_FILE_READ_ISSUE,
    BMP_FILE_WRITE_ISSUE,
    BMP_WRONG_SIGNATURE,
    BMP_NO_RESERVED,
    BMP_UNSUPPORTED_OFFSET,
    BMP_UNSUPPORTED_INFO_FORMAT,
    BMP_UNSUPPORTED_PLANES_COUNT,
    BMP_UNSUPPORTED_BITS_COUNT,
    BMP_UNSUPPORTED_COMPRESSION,
    BMP_UNSUPPORTED_CLR,
    BMP_WONG_SIZE,
    BMP_WONG_FILESIZE,
    BMP_WRONG_PADDINGS,
};

extern const char* bitmap_errmsg[];

struct bitmap* create_blank_bitmap();
enum bitmap_errors try_read_bitmap(struct bitmap* blank_bmp, FILE* file);
enum bitmap_errors try_write_bitmap(struct bitmap* bmp, FILE* file);
void destroy_bitmap(struct bitmap* bmp);
struct bitmap_pixel* get_pixel_ref(struct bitmap* bmp, uint32_t x, uint32_t y);

void mirror_bmp_x(struct bitmap* bmp);
void transpose_bmp(struct bitmap* bmp);

#endif
