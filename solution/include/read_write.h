#ifndef READ_WRITE_H
#define READ_WRITE_H

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

bool read_uint8_t (FILE* file, uint8_t*  output);
bool read_le_uint16_t(FILE* file, uint16_t* output);
bool read_le_uint32_t(FILE* file, uint32_t* output);

bool write_uint8_t (FILE* file, uint8_t  input);
bool write_le_uint16_t(FILE* file, uint16_t input);
bool write_le_uint32_t(FILE* file, uint32_t input);

#endif
